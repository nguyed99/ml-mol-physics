import numpy as np
import torch
import matplotlib.pyplot as plt

from itertools import product
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchmetrics.functional import r2_score, mean_squared_error, mean_absolute_error


DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'

# Helper functions and dataclass
def split_data(x_full: np.ndarray,
               y_full: np.ndarray,
               train_fraction: float):
    """Generates three pytorch Datasets (full, train, test) given a set of features and labels."""

    # define fraction of data used for training
    assert x_full.shape[0] == y_full.shape[0]
    
    n_samples = y_full.shape[0]

    n_train = int(train_fraction * n_samples)

    # get indices for training and test set
    ids = np.arange(n_samples)
    np.random.shuffle(ids)
    ids_train, ids_test  = np.split(ids, [n_train])

    all_data = Data(x_full, y_full)
    train_data = Data(x_full[ids_train], y_full[ids_train])
    test_data = Data(x_full[ids_test], y_full[ids_test])

    return all_data, train_data, test_data

class Data(Dataset):
  """Dataset wrapper. Takes numpy arrays and stores them as torch tensors.
    Data is transformed to the device."""
  def __init__(self, x: np.ndarray,
                y: np.ndarray,
                device: str = DEVICE):
    self.x = torch.from_numpy(x).float().to(device)
    self.y =  torch.from_numpy(y).long().to(device).squeeze()
    self.len = self.x.shape[0]
  
  def __getitem__(self, index: int) -> tuple:
    return self.x[index], self.y[index]
  
  def __len__(self) -> int:
    return self.len



# -------------------
# 1.1
# -------------------
x = np.random.uniform(low = -1, high = 1, size = (10,2))
x1, x2 = np.meshgrid(x[:,0], x[:,1])
x = np.c_[x1.ravel(), x2.ravel()]
# x = x.reshape(100,1,2)
y = np.multiply(x1, x2)
y[y > 0] = 1
y[y <= 0] = 0
y = y.reshape(100,1).astype(int)

all_data, train_data, test_data = split_data(x,y,train_fraction=0.75)
color_map = ['blue' if i == 1 else 'red' for i in y]

# fig, ax = plt.subplots()
# ax.scatter(train_data.x[:,:,0], train_data.x[:,:,1], c=['blue' if i == 1 else 'red' for i in train_data.y], marker='o', label='train')
# ax.scatter(test_data.x[:,:,0], test_data.x[:,:,1], c=['blue' if i == 1 else 'red' for i in test_data.y], marker='x', label='test')
# plt.legend()
# plt.show()


# -------------------
# 1.2
# -------------------

################################ NN class ################################
class MLPClassifier(nn.Module):
    def __init__(self, n_dims: list, activation= nn.ReLU()):
        """
            Simple multi-layer perceptron (MLP) for classification.

            Parameters:
            -----------
            n_dims : List of integers specifying the dimensions of input and output and the hidden layers.
            activation: Activation function used for non-linearity. 
        """
        super().__init__()

        # Get input and output dimensions
        dims_in = n_dims[:-1]
        dims_out = n_dims[1:]

        layers = []

        # Add linear layers (and activation function after all layers except the final one)
        for i, (dim_in, dim_out) in enumerate(zip(dims_in, dims_out)):
            layers.append(torch.nn.Linear(dim_in, dim_out))
            
            if i < len(n_dims) - 2:
                layers.append(activation)
        

        self._layers = torch.nn.Sequential(*layers)

    def forward(self, x):
        """
            MLP forward pass

        """
        return self._layers(x)
    
    def predict_proba(self, x):
        """
        Apply softmax to the output of the MLP to get class probabilities.
        """
        # Apply softmax on the output of the MLP (class probabilities)
        return nn.functional.softmax(self.forward(x), dim=1) # output dimension
    
    def count_parameters(self): 
        """
            Counts the number of trainable parameters.

        """
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

class Trainer:

    def __init__(self, model:nn.Module, train_loader: DataLoader, optimizer: torch.optim.Optimizer, criterion: nn.modules.loss._Loss, n_epochs: int):
        self.model = model
        self.train_loader = train_loader
        self.optimizer = optimizer
        self.criterion = criterion
        self.n_epochs = n_epochs
        self.losses = np.empty(n_epochs)
    def train(self):
        """
            Basic training loop for a pytorch model.

            Parameters:
            -----------
            model : pytorch model.
            train_loader : pytorch Dataloader containing the training data.
            optimizer: Optimizer for gradient descent. 
            criterion: Loss function. 
            

            Example usage:
            -----------

            model = (...) # a pytorch model
            criterion = (...) # a pytorch loss
            optimizer = (...) # a pytorch optimizer
            trainloader = (...) # a pytorch DataLoader containing the training data

            n_epochs = 10000
            for epoch in range(1, n_epochs):
                epoch_loss = train(model, trainloader,optimizer, criterion)
                

        """
            
        # Set model to training mode
        self.model.train()

        # Loop over each batch from the training set
        for epoch in range(self.n_epochs):
            for (data, target) in self.train_loader:
            
                # Copy data to GPU if needed
                data = data.to(DEVICE)
                target = target.to(DEVICE)

                # set optimizer to zero grad to remove previous gradients
                self.optimizer.zero_grad() 

                # Pass data through the network
                output = self.model(data)

                # Calculate loss
                loss = self.criterion(output, target)

                # get gradients
                loss.backward()
                
                # gradient descent
                self.optimizer.step()
                self.losses[epoch] = loss.item()

    def evaluate_classification(self, test_loader: DataLoader) -> dict:
        
        """
            Evaluates a classification model by computing loss and classification accuray on a test set.

            Parameters:
            -----------
            model : pytorch model.
            test_loader : pytorch Dataloader containing the test data.
            criterion: Loss function. 

        """

        self.model.eval()
        
        val_loss, correct = 0, 0
        for data, target in test_loader:
            data = data.to(DEVICE)
            target = target.to(DEVICE)
            output = model(data)
            val_loss += self.criterion(output, target).data.item()

            pred = output.data.max(1)[1] # get the index of the max probability
            correct += pred.eq(target.data).cpu().sum()

        val_loss /= len(test_loader)

        accuracy = 100. * correct.to(torch.float32) / len(test_loader.dataset)
        
        return {'loss': val_loss, 'accuracy': accuracy}

# -------------------
# 1.3
# -------------------
learning_rate = 0.01
batch_size = 16
n_epochs = 1000
trainloader = DataLoader(train_data, 
                         batch_size=batch_size, 
                         shuffle=True)
testloader = DataLoader(test_data, 
                         batch_size=batch_size, 
                         shuffle=True)

no_of_hidden_layers = np.arange(10, step=5) + 1
no_of_nodes = np.arange(100, step=20) + 1

for NN in product(no_of_hidden_layers, no_of_nodes):
    dim_hidden_layers, dim_nodes = NN
    print(f'{dim_hidden_layers=}; {dim_nodes=}')
    # dim_in=2 <- (x1,x2); dim_out=1 <- {0,1}
    dimensions = [2] + [dim_nodes]*dim_hidden_layers + [2]
    model = MLPClassifier(n_dims=dimensions, activation=nn.ReLU()).to(DEVICE)
    # optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    trainer = Trainer(model, train_loader=trainloader, optimizer=optimizer, criterion=nn.CrossEntropyLoss(), n_epochs=n_epochs)
    trainer.train()

    loss, accuracy = trainer.evaluate_classification(test_loader=testloader).values()
    print(f'{loss=}; {accuracy=}')
