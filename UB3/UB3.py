import numpy as np
import matplotlib.pyplot as plt
import torch
from itertools import product
from torch import nn
from torch.utils.data import Dataset, DataLoader
from torchmetrics.functional import r2_score, mean_squared_error, mean_absolute_error

DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'


def f(x: float) -> float:
    return x + 1.5*x**2 + 0.5*x**3 -0.7*x**4 + np.sin(5*x) + np.cos(10*x)

def split_data(x_full: np.ndarray,
               y_full: np.ndarray,
               train_fraction: float):
    """Generates three pytorch Datasets (full, train, test) given a set of features and labels."""

    # define fraction of data used for training
    assert x_full.shape[0] == y_full.shape[0]
    
    n_samples = y_full.shape[0]

    n_train = int(train_fraction * n_samples)

    # get indices for training and test set
    ids = np.arange(n_samples)
    np.random.shuffle(ids)
    ids_train, ids_test  = np.split(ids, [n_train])

    all_data = Data(x_full, y_full)
    train_data = Data(x_full[ids_train], y_full[ids_train])
    test_data = Data(x_full[ids_test], y_full[ids_test])

    return all_data, train_data, test_data

class Data(Dataset):
  """Dataset wrapper. Takes numpy arrays and stores them as torch tensors.
    Data is transformed to the device."""
  def __init__(self, x: np.ndarray,
                y: np.ndarray,
                device: str = DEVICE):
    self.x = torch.from_numpy(x).float().to(device)
    self.y =  torch.from_numpy(y ).float().to(device)
    self.len = self.x.shape[0]
  
  def __getitem__(self, index: int) -> tuple:
    return self.x[index], self.y[index]
  
  def __len__(self) -> int:
    return self.len

################################ NN class ################################
class MLP(nn.Module):
    def __init__(self, n_units: list, activation= nn.ReLU()):
        """
            Simple multi-layer perceptron (MLP).

            Parameters:
            -----------
            n_units : List of integers specifying the dimensions of input and output and the hidden layers.
            activation: Activation function used for non-linearity. 


            Example:
            -----------

            dim_hidden = 100
            dim_in = 2
            dim_out = 5

            # MLP with input dimension 2, output dimension 5, and 4 hidden layers of dimension 100
            model = MLP([dim_in,
                        dim_hidden,
                        dim_hidden,
                        dim_hidden,
                        dim_hidden,
                        dim_out],activation=nn.ReLU()).to(DEVICE)

        """
        super().__init__()

        # Get input and output dimensions
        dims_in = n_units[:-1]
        dims_out = n_units[1:]

        layers = []

        # Add linear layers (and activation function after all layers except the final one)
        for i, (dim_in, dim_out) in enumerate(zip(dims_in, dims_out)):
            layers.append(torch.nn.Linear(dim_in, dim_out))
            
            if i < len(n_units) - 2:
                layers.append(activation)
        

        self._layers = torch.nn.Sequential(*layers)

    def forward(self, x):
        """
            MLP forward pass

        """
        return self._layers(x)
    
    def count_parameters(self): 
        """
            Counts the number of trainable parameters.

        """
        return sum(p.numel() for p in self.parameters() if p.requires_grad)

################################
################################ Trainer class ################################

class Trainer:

    def __init__(self, model:nn.Module, train_loader: DataLoader, optimizer: torch.optim.Optimizer, criterion: nn.modules.loss._Loss, n_epochs: int):
        self.model = model
        self.train_loader = train_loader
        self.optimizer = optimizer
        self.criterion = criterion
        self.n_epochs = n_epochs
        self.losses = np.empty(n_epochs)
    
    def train(self):
        """
            Basic training loop for a pytorch model.

            Parameters:
            -----------
            model : pytorch model.
            train_loader : pytorch Dataloader containing the training data.
            optimizer: Optimizer for gradient descent. 
            criterion: Loss function. 
            

            Example usage:
            -----------

            model = (...) # a pytorch model
            criterion = (...) # a pytorch loss
            optimizer = (...) # a pytorch optimizer
            trainloader = (...) # a pytorch DataLoader containing the training data

            n_epochs = 10000
            for epoch in range(1, n_epochs):
                epoch_loss = train(model, trainloader,optimizer, criterion)
                

        """
            
        # Set model to training mode
        self.model.train()

        # Loop over each batch from the training set
        for epoch in range(self.n_epochs):
            for (data, target) in self.train_loader:
            
                # Copy data to GPU if needed
                data = data.to(DEVICE)
                target = target.to(DEVICE)

                # set optimizer to zero grad to remove previous gradients
                self.optimizer.zero_grad() 

                # Pass data through the network
                output = self.model(data)

                # Calculate loss
                loss = self.criterion(output, target)

                # get gradients
                loss.backward()
                
                # gradient descent
                self.optimizer.step()
                self.losses[epoch] = loss.item()


################################

# -------------------
# 1.1
# -------------------

x = np.linspace(-1, 2, 100).reshape(-1, 1)
y = f(x)

all_data, train_data, test_data = split_data(x,y,train_fraction=0.75)
plt.figure()
plt.plot(x, y, label='all')
plt.plot(train_data.x, train_data.y, 'x', label='train')
plt.plot(test_data.x, test_data.y, 'v', label='test')

plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.tight_layout()
plt.grid()
plt.savefig('UB3_1.1_split.png')

# -------------------
# 1.2 a)
# -------------------

learning_rate = 0.01
batch_size = 16
n_epochs = 1000
trainloader = DataLoader(train_data, 
                         batch_size=batch_size, 
                         shuffle=True)
testloader = DataLoader(test_data, 
                         batch_size=batch_size, 
                         shuffle=True)

no_of_hidden_layers = np.arange(10, step=5) + 1
no_of_nodes = np.arange(100, step=20) + 1

MSE={no_of_hidden_layers[0]:{'training_set':[],
                             'testing_set':[]}, 
    no_of_hidden_layers[1]:{'training_set':[],
                             'testing_set':[]}}
for NN in product(no_of_hidden_layers, no_of_nodes):
    # dim_in=1 <- x; dim_out=1 <- f(x)
    dim_hidden_layers, dim_nodes = NN
    print(f'{dim_hidden_layers=}, {dim_nodes=}')
    dimensions = [1] + [dim_nodes]*dim_hidden_layers + [1]
    model = MLP(n_units=dimensions, activation=nn.ReLU()).to(DEVICE)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
        
    #  determine the MSE for the training dataset
    trainer = Trainer(model, train_loader=trainloader, optimizer=optimizer, criterion=nn.MSELoss(), n_epochs=n_epochs)
    trainer.train()
    
    print(f'MSE of training set = {mean_squared_error(train_data.y, trainer.model(train_data.x)):8.3f}')
    MSE[dim_hidden_layers]['training_set'].append(float(mean_squared_error(train_data.y, trainer.model(train_data.x)).detach().numpy()))

    #  determine the MSE for the testing dataset
    print(f'MSE of testing set = {mean_squared_error(test_data.y, trainer.model(test_data.x)):8.3f}')
    MSE[dim_hidden_layers]['testing_set'].append(float(mean_squared_error(test_data.y, trainer.model(test_data.x)).detach().numpy()))

for k,v in MSE.items():
    plt.figure()
    plt.yscale("log")   
    plt.plot(no_of_nodes, v['training_set'], 'o', label='train')
    plt.plot(no_of_nodes, v['testing_set'], 'o', label='test')
    plt.xlabel('Number of nodes/layer')
    plt.ylabel('MSE')
    plt.legend()
    plt.title(f'No of hidden layers = {k}')
    plt.tight_layout()
    plt.grid()
    plt.savefig(f'UB3_1.2_MSE_{k}.png')


## optimal network
dim_hidden_layers, dim_nodes = (6,41)
dimensions = [1] + [dim_nodes]*dim_hidden_layers + [1]
model = MLP(n_units=dimensions, activation=nn.ReLU()).to(DEVICE)
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    
trainer = Trainer(model, train_loader=trainloader, optimizer=optimizer, criterion=nn.MSELoss(), n_epochs=n_epochs)

## losses as function of n_epochs
plt.figure()
plt.plot(trainer.losses)
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.tight_layout()
plt.grid()
plt.savefig(f'UB3_1.2_loss.png')

# -------------------
# 1.2 b)
# -------------------

plt.figure()
plt.plot(x, y, label='analytical')
plt.plot(train_data.x, trainer.model(train_data.x).detach().numpy(), 'x', label='train')
plt.plot(test_data.x, trainer.model(test_data.x).detach().numpy(), 'v', label='test')

plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.tight_layout()
plt.grid()
plt.savefig('UB3_1.2_b_f_train_test.png')

## entire range
x_tensor = torch.from_numpy(x).float().to(DEVICE)
plt.figure()
plt.plot(x, y, label='analytical')
plt.plot(x_tensor, trainer.model(x_tensor).detach().numpy(), 'x', label='NN')

plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.tight_layout()
plt.grid()
plt.savefig('UB3_1.2_b_f_NN.png')

##  For test dataset: compute $R^2$, MSE, MAE

print(f'MSE of testing set = {mean_squared_error(test_data.y, trainer.model(test_data.x)):8.3f}')
print(f'MAE of testing set = {mean_absolute_error(test_data.y, trainer.model(test_data.x)):8.3f}')
print(f'R^2 of testing set = {r2_score(test_data.y, trainer.model(test_data.x)):8.3f}')

###  For train dataset: compute MSE, MAE
print(f'MSE of train set = {mean_squared_error(train_data.y, trainer.model(train_data.x)):8.3f}')
print(f'MAE of train set = {mean_absolute_error(train_data.y, trainer.model(train_data.x)):8.3f}')