import numpy as np
import matplotlib.pyplot as plt

import torch
from torchmetrics.functional import r2_score, mean_squared_error, mean_absolute_error
from torch import nn
from torch.utils.data import Dataset, DataLoader, random_split
from sklearn import datasets

DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'

# data set params
npoints = 500
min_val = 0
max_val = 50
x = np.linspace(min_val, max_val, npoints)
train_fraction = .8
n_train = int(train_fraction * npoints)
ids = np.arange(npoints)
batch_size = 16

# model params
class LinearRegression(torch.nn.Module):
    def __init__(self):
        super(LinearRegression, self).__init__() # method to intialize the parent class torch.nn.Module
        # defines a linear layer
        self.linear = torch.nn.Linear(in_features=1, # each input to the layer is a scalar (1D tensor)
                                      out_features=1,bias=True) # bias=True => b is a learnable parameter

    def forward(self, x):
        out = self.linear(x)
        return out
    

m = 2.0
b = 5.00
model = LinearRegression().to(DEVICE)

# training params
learning_rate = 0.01
n_epochs = 1000
n_log = 50 # logging interval

# training functions
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate) # stochastic gradient descent

# def f_linear(x, m, b):
#     return m * x + b


class Data(Dataset):
  """Dataset wrapper. Takes numpy arrays and stores them as torch tensors.
    Data is transformed to the device."""
  def __init__(self, x: np.ndarray,
                y: np.ndarray,
                device: str = DEVICE):
    self.x = torch.from_numpy(x).float().to(device)
    self.y =  torch.from_numpy(y).float().to(device)
    self.len = self.x.shape[0]
  
  def __getitem__(self, index: int) -> tuple:
    return self.x[index].float(), self.y[index].float()
  
  def __len__(self) -> int:
    return self.len


# class PTMinMaxScaler(object):
#     """"Scaler class for min-max scaling."""
#     def __init__(self, tensor, new_max, new_min):
#         self.v_min, self.v_max = tensor.min(), tensor.max()
#         self.new_max, self.new_min = new_max, new_min

#     def scale(self, tensor):
#         return (tensor - self.v_min)/(self.v_max - self.v_min)*(self.new_max - self.new_min) + self.new_min

#     def inverse_scale(self, tensor):
#         return (tensor - self.new_min) * (self.v_max - self.v_min)/(self.new_max - self.new_min) + self.v_min


  


# x_scaler = PTMinMaxScaler(x,1., 0.)

# x_scaled = x_scaler.scale(x)

# ####################################################################
# dmax = [1,10,20]

# for i in dmax:
#     print(f'dmax={i}; {min_val=}; {max_val=}; {learning_rate=}')
#     y = f_linear(x, m, b) + np.random.randn(npoints) * i
#     y_scaler = PTMinMaxScaler(y,1., 0.)

#     y_scaled = y_scaler.scale(y)

#     x_scaled = x_scaled.reshape(-1,1)
#     y_scaled = y_scaled.reshape(-1,1)

#     np.random.shuffle(ids)
#     ids_train, ids_test  = np.split(ids, [n_train])
#     train_data = Data(x_scaled[ids_train], y_scaled[ids_train])
#     test_data = Data(x_scaled[ids_test], y_scaled[ids_test])

#     trainloader = DataLoader(train_data, 
#                          batch_size=batch_size, 
#                          shuffle=True)
    
#     losses = np.empty(n_epochs)
#     for epoch in range(n_epochs):
#         for _, (inputs, labels) in enumerate(trainloader):
#             outputs = model(inputs)
            
#             # compute loss
#             loss = criterion(outputs, labels)   
            
#             # set optimizer to zero grad to remove previous gradients
#             optimizer.zero_grad()    # backward propagation
            
#             # get gradients
#             loss.backward()

#             # gradient descent
#             optimizer.step() 
            
#             losses[epoch] = loss.item() # store loss
#         # if epoch % n_log == 0:
#         #     print(f'Epochs:{epoch :5d} | ' \
#         #   f'Loss: {loss :.10f}')
    
#     with torch.no_grad(): # do not use gradients to update the model
#         y_pred_test = model(test_data.x)
    
#     y_scaler = PTMinMaxScaler(y_pred_test, 1. , 0.)
#     y_pred_test = y_scaler.inverse_scale(y_pred_test)

#     test_data_y_scaler = PTMinMaxScaler(test_data.y, 1. , 0.)
#     test_data_y = test_data_y_scaler.inverse_scale(test_data.y)
#     # compute R^2
#     print('R2 score = %8.5f' % r2_score(y_pred_test, test_data_y))

#     # compute MAE & MSE
#     mae_test = mean_absolute_error(test_data_y,y_pred_test)
#     mse_test = mean_squared_error(test_data_y,y_pred_test)
#     print('MSE test  = %8.4f    MAE test  = %8.4f' % (mse_test,mae_test))
    
    
    # plt.figure()
    # plt.plot(test_data.x, test_data.y, 'x', label = 'test')

    # Plot the predicted data 
    # plt.plot(test_data.x, y_pred_test,c="blue", label = 'predictions',linewidth=2)
    # plt.plot(x,y_orig,  label = 'linear',c="red",linestyle="dashed",linewidth=2) # y_orig = f_linear(x, m, b)
    # plt.legend(fontsize=15)
    # plt.title(f'dmax={i}; {min_val=}; {max_val=}; {learning_rate=}')
    # plt.xlabel('x', fontsize=15)
    # plt.ylabel('y', fontsize=15)
    # plt.xticks(fontsize=12)
    # plt.yticks(fontsize=12)
    # plt.savefig(f'UB2_1.2.2_dmax={i}_{min_val=}_{max_val=}_{learning_rate=}.png')

#################################################################### section 2
diabetes_X, diabetes_y = datasets.load_diabetes(return_X_y=True)
xcol=['age','sex','body mass index','average blood pressure','total serum cholesterol','low-density lipoproteins','high-density lipoproteins','total_cholesterol','serum triglycerides level','blood surgar level']

data_y = diabetes_y
plt.figure()
plt.hist(data_y, bins = len(data_y), density=True)
plt.title(f'y values')
plt.tight_layout()

plt.savefig(f'UB2_2.2_distribution_y_val.png')


# for i_col in range(len(xcol)): # loop over 10 attributes
#   data_x = diabetes_X[:,i_col]
#   data_y = diabetes_y
#   ndata = len(data_x)

#   ## data analysis
#   # print(f'{np.mean(data_x)=}')
#   # print(f'{np.var(data_x)=}')

#    # plot data distribution of the 10 attributes
#   plt.figure()
#   # plt.hist(data_x, bins = len(data_x), density=True)
#   plt.hist(data_y, bins = len(data_y), density=True)

#   plt.title(f'y values')
#   plt.tight_layout()

#   plt.savefig(f'UB2_2.2_distribution_attribute={xcol[i_col]}.png')


  # data_x = data_x.reshape(-1,1)
  # data_y =data_y.reshape(-1,1)
  # train_fraction = .9
  # n_train = int(train_fraction * ndata)
  # ids = np.arange(ndata)

  # ids_train, ids_test  = np.split(ids, [n_train])
  # train_data = Data(data_x[ids_train], data_y[ids_train])
  # test_data = Data(data_x[ids_test], data_y[ids_test])

 

  # trainloader = DataLoader(train_data, 
  #                       batch_size=batch_size, 
  #                       shuffle=True)
  
  # for epoch in range(n_epochs):
  #   for _, (inputs, labels) in enumerate(trainloader):
  #       outputs = model(inputs)
        
  #       # compute loss
  #       loss = criterion(outputs, labels)   
        
  #       # set optimizer to zero grad to remove previous gradients
  #       optimizer.zero_grad()    # backward propagation
        
  #       # get gradients
  #       loss.backward()

  #       # gradient descent
  #       optimizer.step()

  # with torch.no_grad(): # do not use gradients to update the model
  #       y_pred_test = model(test_data.x)
  
  # plt.figure()
  # plt.plot(test_data.x, test_data.y, 'x', label = 'test')
  # plt.plot(test_data.x, y_pred_test,c="blue", label = 'predictions',linewidth=2)
  # plt.legend(fontsize=10)
  # plt.xlabel('x', fontsize=15)
  # plt.ylabel('y', fontsize=15)
  # plt.title(f'attribute={xcol[i_col]}')
  # plt.savefig(f'UB2_2.1_fittin_attribute={xcol[i_col]}.png')



  


